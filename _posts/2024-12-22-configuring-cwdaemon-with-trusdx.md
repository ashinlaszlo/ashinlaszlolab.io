---
layout: post
author: laszlo
---

The ham radio logger program xlog supports CW keying but I needed to figure
out how to enable it. I use Debian stable which had cwdaemon 0.10.2 which
doesn't give option for configuring serial port control lines and trusdx
doesn't use the default setup. I needed to manually build the latest
cwdaemon (0.13.0) to have the `-o` flag.

Here's the command line I needed to start cwdaemon with:

```
./cwdaemon -d ttyUSB0 -x n -o ptt=none -o key=RTS
```

It is super useful to know that both cwdaemon and rigctl (hamlib) can
use the same device file at the same time (one is only pulling the control
lines, the other one issues CAT commands)

Currently xlog spins up rigctl but I want to figure out if it recognizes
an already running daemon because I don't want to have the radio reset
every time I restart xlog, and also I may have multiple programs wanting
to access the radio.

hamlib serial config params for Trusdx:

```
serial_speed=115200,rts_state=OFF,dtr_state=ON
```
