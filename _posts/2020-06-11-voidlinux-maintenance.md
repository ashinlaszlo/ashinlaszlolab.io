---
layout: post
author: laszlo
---
One day I was unable to install updates because all the free space
disappeared from my rootfs. AFAIK I shouldn't have too many large stuff
stored on this file system so I decided to figure out what is going on.

## Listing installed voidlinux packages ordered by size

This should be an easy task but it took a while for me to figure out how
to actually do it:

	xbps-query --regex -p installed_size -s '.*' | sort -k2 -h

With this I found arm cross gcc toolchain was installed but as long as I
use docker for devenv isolation I don't actually need this any more.

## Purging old kernels

As `ncdu` pointed out I have several versions of the linux kernel installed
(headers and modules). voidlinux keeps those to be safe. There is no
automated way that xbps uses to take care of these but there is a tool
named `vkpurge`:

	sudo vkpurge rm all

(This won't delete the current version.)

I used this tool but it left out most of the files from the 4.19.x linux
kernel series. Using `xlocate` I confirmed those were orphaned, so I
removed them by using direct commands.

With all these I managed to free up **11GB** disk space! A lot of unneeded
stuff was hanging around under `/usr/lib/modules` and `/usr/src`.
